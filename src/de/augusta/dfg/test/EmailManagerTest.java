// TODO: fix gradle build script || use other test framework

//package de.augusta.dfg.test;
//
//import de.augusta.dfg.model.MockManager;
//import de.augusta.dfg.model.EmailManager;
//import de.augusta.dfg.model.object.Person;
//import org.testng.Assert;
//import org.testng.annotations.Test;
//
//import java.util.List;
//import java.util.stream.Collectors;
//
//public class EmailManagerTest {
//    @Test
//    public void emailList() throws Exception {
//        final double maxMailCount = 50.0;
//
//        List<Person> tempData = MockManager.createPersons(666);
//        List<Person> testData = tempData.stream().filter(person -> person.hasEmail()).collect(Collectors.toList());
//        int expectedMailCount = testData.size();
//
//        double temp = expectedMailCount / maxMailCount;
//        int expectedLinkCount = (int) Math.ceil(temp);
//
//        List<String> actualMailResult = EmailManager.getInstance().emailList(testData, "test");
//
//        Assert.assertEquals(actualMailResult.size(), expectedLinkCount, "Es wurde nicht die erwartete Anzahl an mailTo-Links gefunden!");
//
//        for (String row : actualMailResult) {
//            int rowCount = row.split("@", -1).length - 1;
//            String assertMessage = String.format("Zu viele e-Mails pro Zeile gefunden! (erwartet: %d, test daten: %d)", (int) maxMailCount, rowCount);
//            Assert.assertTrue(rowCount <= (int) maxMailCount, assertMessage);
//        }
//    }
//
//}