package de.augusta.dfg;

import de.augusta.dfg.model.ArgumentUtils;
import de.augusta.dfg.model.ConfigManager;
import de.augusta.dfg.model.PersonManager;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {
        final double minWidth = 720;
        final double minHeight = 620;

        Parent root = FXMLLoader.load(getClass().getResource("view/MainView.fxml"));
        primaryStage.setTitle(createTitle());
        primaryStage.setScene(new Scene(root, minWidth, minHeight));
        primaryStage.getScene().getStylesheets().add(getClass().getResource("view/default.css").toExternalForm());
        primaryStage.setMinWidth(minWidth);
        primaryStage.setMinHeight(minHeight);
        primaryStage.show();
    }

    public static void main(String[] args) {
        ConfigManager.getInstance().setArguments(new ArgumentUtils(args));

        PersonManager.getInstance().load();

        launch(args);
    }

    @SuppressWarnings("SpellCheckingInspection")
    private String createTitle()
    {
        String title = "DFG Verwaltung";

        if(ConfigManager.getInstance().isMockUpMode()) {
            title += " +++ MOCKUP MODE";
        }

        if(ConfigManager.getInstance().isDebugMode()){
            title += " +++ DEBUG MODE";
        }

        return title;
    }
}
