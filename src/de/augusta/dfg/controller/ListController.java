package de.augusta.dfg.controller;

import de.augusta.dfg.model.PrintManager;
import de.augusta.dfg.model.object.Person;
import javafx.collections.ObservableList;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

public class ListController implements Initializable {
    private final List<Person> persons;
    public CheckBox checkBoxFilterMember;
    public TableView<Person> tableView;
    public Label labelStatus;
    public TextField textFieldComment;

    ListController(List<Person> persons) {
        this.persons = persons;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        updateTableView();
    }

    public void filterUpdateEvent() {
        updateTableView();
    }

    public void btCancelClicked(MouseEvent mouseEvent) {
        ((Node) (mouseEvent.getSource())).getScene().getWindow().hide();
    }

    private void updateTableView() {
        boolean isMemberFilter = checkBoxFilterMember.isSelected();

        ObservableList<Person> tableViewItems = tableView.getItems();
        tableViewItems.clear();

        if(isMemberFilter) {
            tableViewItems.addAll(persons.stream().filter(person -> person.isMember()).collect(Collectors.toList()));
        }
        else {
            tableViewItems.addAll(persons);
        }

        updateStatusText();
    }

    @SuppressWarnings("SpellCheckingInspection")
    private void updateStatusText() {
        String status = String.format("Anzahl: %d", tableView.getItems().size());
        labelStatus.setText(status);
    }

    public void buttonPrintPersonDetailsClicked() {
        PrintManager.getInstance().printPersonDetails(tableView.getItems());
    }

    public void btPrintMembershipFeeList() {
        PrintManager.getInstance().printMembershipFeeList(tableView.getItems());
    }

    public void btPrintList() {
        PrintManager.getInstance().printList(tableView.getItems());
    }

    public void btPrintPersonList() {
        PrintManager.getInstance().printPersonList(tableView.getItems());
    }
}
