package de.augusta.dfg.controller;

import de.augusta.dfg.model.MembershipFeeManager;
import de.augusta.dfg.model.PersonManager;
import de.augusta.dfg.model.object.MembershipFee;
import de.augusta.dfg.model.object.Person;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;

import java.net.URL;
import java.util.Calendar;
import java.util.List;
import java.util.ResourceBundle;

public class MembershipFeeController implements Initializable {
    @FXML
    public TextField textFieldYear;
    public TextField textFieldFilter;
    public TableView<MembershipFee> tableViewMembershipFeeItems;

    private List<Person> persons;
    private boolean editCanceled;

    MembershipFeeController(List<Person> persons) {
        this.persons = persons;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        initViewData();
    }

    public void onYearFilterChanged() {
        String year = textFieldYear.getText();

        if(!inputIsValid(year)) {
            return;
        }

        updateViewData(year);
    }

    public void onMembershipFeeTableClicked(MouseEvent mouseEvent) {
        if(mouseEvent.getButton().equals(MouseButton.PRIMARY) && mouseEvent.getClickCount() == 2) {
            MembershipFee membershipFee = tableViewMembershipFeeItems.getFocusModel().getFocusedItem();
            if (membershipFee != null) {
                membershipFee.setPayed(!membershipFee.isPayed());
                int index = tableViewMembershipFeeItems.getItems().indexOf(membershipFee);
                if(index == -1) {
                    return;
                }

                tableViewMembershipFeeItems.getItems().set(index, membershipFee);

                updatePerson(membershipFee);
            }
        }
    }

    public void btCancelClicked(MouseEvent mouseEvent) {
        this.editCanceled = true;
        MembershipFeeManager.getInstance().resetCache();
        ((Node) (mouseEvent.getSource())).getScene().getWindow().hide();
    }

    public void btOkClicked(MouseEvent mouseEvent) {
        this.editCanceled = false;
        ((Node) (mouseEvent.getSource())).getScene().getWindow().hide();
    }

    public void onYearFilterReset() {
        textFieldFilter.setText("");
    }

    private void initViewData() {
        Calendar today = Calendar.getInstance();
        int year = today.get(Calendar.YEAR);
        textFieldYear.setText(String.valueOf(year));

        updateViewData(Integer.toString(year));
    }

    private void updateViewData(String year) {
        ObservableList<MembershipFee> membershipFeeList = tableViewMembershipFeeItems.getItems();
        membershipFeeList.clear();
        for(Person person : this.persons) {
            membershipFeeList.add(MembershipFeeManager.getInstance().getOrCreateMembershipFee(person, year));
        }
    }

    private boolean inputIsValid(String input) {
        if(input.length() != 4) {
            return false;
        }

        try {
            //noinspection ResultOfMethodCallIgnored
            Integer.parseInt(input);
            return true;
        }
        catch(Exception e) {
            return false;
        }
    }

    private void updatePerson(MembershipFee membershipFee) {
        int personId = membershipFee.getPersonId();
        Person updatePerson = persons.stream().findFirst().filter(person -> person.getId() == personId).orElse(null);

        if (updatePerson == null) {
            return;
        }

        updatePerson.updateMembershipFee(membershipFee);

        PersonManager.getInstance().editPerson(updatePerson);
    }

    public boolean isEditCanceled() {
        return editCanceled;
    }
}
