package de.augusta.dfg.controller;

import de.augusta.dfg.model.ConfigManager;
import de.augusta.dfg.model.EmailManager;
import de.augusta.dfg.model.enums.EmailFilter;
import de.augusta.dfg.model.object.Person;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

public class EmailController implements Initializable {
    public TextField textFieldEmailSubject;
    private EmailFilter emailFilter;
    private final List<Person> persons;
    public TableView<Person> tableView;
    public Label labelStatus;

    EmailController(List<Person> persons) {
        this.persons = new ArrayList<>();
        this.persons.addAll(persons.stream().filter(person -> person.hasEmail()).collect(Collectors.toList()));
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        emailFilter = EmailFilter.All;
        textFieldEmailSubject.setText(ConfigManager.getInstance().getEmailSubject(emailFilter));
        updateTableView();
    }

    @SuppressWarnings("SpellCheckingInspection")
    public void rbFilterChanged(MouseEvent mouseEvent) {
        RadioButton radioButton = (RadioButton) mouseEvent.getSource();

        switch (radioButton.getText()) {
            case "Alle":
                emailFilter = EmailFilter.All;
                break;
            case "Mitglieder":
                emailFilter = EmailFilter.Member;
                break;
            case "Lehrer":
                emailFilter = EmailFilter.Teacher;
                break;
            case "Info-Verteiler":
                emailFilter = EmailFilter.InfoList;
                break;
            case "Interesierte":
                emailFilter = EmailFilter.Interested;
                break;
            default:
                throw new IllegalArgumentException("Unknon email filter! " + radioButton.getText());
        }

        textFieldEmailSubject.setText(ConfigManager.getInstance().getEmailSubject(emailFilter));

        updateTableView();
    }

    public void btCancelClicked(MouseEvent mouseEvent) {
        ((Node) (mouseEvent.getSource())).getScene().getWindow().hide();
    }

    public void btEmailSelection() {
        List<Person> selectedPersons = tableView.getSelectionModel().getSelectedItems();

        if (selectedPersons == null || selectedPersons.size() == 0) {
            return;
        }

        List<String> mailToLinks = EmailManager.getInstance().emailList(selectedPersons, textFieldEmailSubject.getText());
        EmailManager.getInstance().sendMails(mailToLinks);
    }

    public void btEmailAll() {
        List<Person> items = tableView.getItems();
        List<String> mailToLinks = EmailManager.getInstance().emailList(items, textFieldEmailSubject.getText());
        EmailManager.getInstance().sendMails(mailToLinks);
    }

    public void btSaveSubjectClicked() {
        ConfigManager.getInstance().setEmailSubject(emailFilter, textFieldEmailSubject.getText());
    }

    private void updateTableView() {
        List<Person> tableViewItems = tableView.getItems();
        tableViewItems.clear();

        for (Person person : persons) {
            switch (emailFilter) {
                case All:
                    tableViewItems.add(person);
                    break;
                case InfoList:
                    if (person.isGroupInfoList()) {
                        if (!tableViewItems.contains(person)) {
                            tableViewItems.add(person);
                        }
                    }
                case Interested:
                    if (person.isGroupInterests()) {
                        if (!tableViewItems.contains(person)) {
                            tableViewItems.add(person);
                        }
                    }
                case Teacher:
                    if (person.isGroupTeacher()) {
                        if (!tableViewItems.contains(person)) {
                            tableViewItems.add(person);
                        }
                    }
                case Member:
                    if (person.isMember()) {
                        if (!tableViewItems.contains(person)) {
                            tableViewItems.add(person);
                        }
                    }
                case Pm:
                    if (person.isGruppePm()) {
                        if (!tableViewItems.contains(person)) {
                            tableViewItems.add(person);
                        }
                    }
            }
        }


        updateStatusText();
    }

    @SuppressWarnings("SpellCheckingInspection")
    private void updateStatusText() {
        String status = String.format("Anzahl: %d", tableView.getItems().size());
        labelStatus.setText(status);
    }
}
