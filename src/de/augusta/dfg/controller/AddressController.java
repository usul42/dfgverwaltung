package de.augusta.dfg.controller;

import de.augusta.dfg.model.PrintManager;
import de.augusta.dfg.model.object.Person;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableView;
import javafx.scene.input.MouseEvent;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

public class AddressController implements Initializable {
    private final List<Person> persons;

    @FXML
    public TableView<Person> tableView;
    public CheckBox checkBoxFilterMember;
    public CheckBox checkBoxFilterNoEmail;
    public Label labelStatus;

    AddressController(ObservableList<Person> items) {
        persons = items;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        updateTableView();
    }

    public void btCancelClicked(MouseEvent mouseEvent) {
        ((Node) (mouseEvent.getSource())).getScene().getWindow().hide();
    }

    public void filterUpdateEvent() {
        updateTableView();
    }

    public void btPrintClicked() {
        if (tableView.getItems().size() == 0) {
            return;
        }

        List<Person> data;

        List<Person> selectedItems = tableView.getSelectionModel().getSelectedItems();
        if (selectedItems.size() == 0) {
            data = tableView.getItems();
        } else {
            data = selectedItems;
        }

        PrintManager.getInstance().printAddresses(data);
    }

    private void updateTableView() {
        boolean isFilterMember = checkBoxFilterMember.isSelected();
        boolean isFilterNoEmail = checkBoxFilterNoEmail.isSelected();

        ObservableList<Person> tableViewItems = tableView.getItems();
        tableViewItems.clear();

        for (Person person : persons) {
            if (isFilterMember) {
                if (!person.isMember()) {
                    continue;
                }
            }
            if (isFilterNoEmail) {
                if (person.getEmail() != null && !person.getEmail().isEmpty()) {
                    continue;
                }
            }

            tableViewItems.add(person);
        }

        updateStatusText();
    }

    @SuppressWarnings("SpellCheckingInspection")
    private void updateStatusText() {
        String status = String.format("Anzahl: %d", tableView.getItems().size());
        labelStatus.setText(status);
    }
}
