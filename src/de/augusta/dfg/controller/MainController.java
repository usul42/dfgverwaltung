package de.augusta.dfg.controller;

import de.augusta.dfg.model.PersonManager;
import de.augusta.dfg.model.object.Person;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.net.URL;
import java.util.Iterator;
import java.util.List;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

public class MainController implements Initializable {
    private Person currentPerson;

    @FXML
    public ListView<Person> personListView;

    // Toolbar Buttons
    public Button buttonCancel;
    public Button buttonEdit;
    public Button buttonCommit;
    public Button buttonNew;
    public Button buttonDelete;
    public Button buttonAddresses;
    public Button buttonLists;
    public Button buttonEmail;
    public Button buttonMembershipFee;

    // Mitgliedfilter und Suche
    public ToggleButton toggleButtonMemberFilter;
    public TextField textFieldSearchInput;
    public Button buttonResetSearch;

    // Personen Daten
    public TextField textFieldFirstName;
    public TextField textFieldLastName;
    public TextField textFieldTitle;
    public TextField textFieldTelephone;
    public TextField textFieldTelephone2;
    public TextField textFieldEmail;
    public TextField textFieldAddress;
    public TextField textFieldZipCode;
    public TextField textFieldCity;
    public DatePicker datePickerEntryDate;
    public TextField textFieldContactPerson;
    public TextArea textAreaNote;
    public CheckBox checkBoxMember;
    public DatePicker datePickerBirthDate;
    public CheckBox checkBoxGroupPm;
    public CheckBox checkBoxGroupTeacher;
    public CheckBox checkBoxGroupInfoList;
    public CheckBox checkBoxGroupInterests;

    // Status Text
    public Label labelStatus;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        updatePersonListView();
        setEditMode(false);
        buttonEdit.setDisable(true);
        buttonDelete.setDisable(true);
    }

    public void mainWindowOnKeyPressed(KeyEvent keyEvent) {
        if (keyEvent.getCode() == KeyCode.Q && keyEvent.isControlDown() && !keyEvent.isAltDown()) {
            System.exit(0);
        }
    }

    public void selectPersonEvent() {
        currentPerson = personListView.getFocusModel().getFocusedItem();
        updateView();
        buttonEdit.setDisable(false);
        buttonDelete.setDisable(false);
    }

    public void btEditClicked() {
        setEditMode(true);
    }

    public void btCancelClicked() {
        updateView();
        setEditMode(false);
    }

    public void btCommitClicked() {
        setEditMode(false);
        updateCurrentPerson();
        if (currentPerson.getId() == 0) {
            PersonManager.getInstance().addPerson(currentPerson);
        } else {
            PersonManager.getInstance().editPerson(currentPerson);
        }
        updatePersonListView();
        PersonManager.getInstance().save();
    }

    public void btNewClicked() {
        currentPerson = new Person();
        updateView();
        setEditMode(true);
    }

    public void btDeleteClicked() {
        if (currentPerson == null) {
            return;
        }

        PersonManager.getInstance().delPerson(currentPerson);
        currentPerson = null;
        updatePersonListView();
        updateView();

        buttonDelete.setDisable(true);
        buttonEdit.setDisable(true);
    }

    public void onMemberSelected() {
        datePickerEntryDate.setDisable(!checkBoxMember.isSelected());
    }

    public void btMemberFilterClicked() {
        updatePersonListView();
    }

    public void btResetSearchClicked() {
        textFieldSearchInput.clear();
        updatePersonListView();
    }

    public void onSearchInputChanged() {
        if (textFieldSearchInput.getText().length() <= 2) {
            updatePersonListView();
            return;
        }

        updatePersonListView(textFieldSearchInput.getText());
    }

    public void btAddressClicked(MouseEvent mouseEvent) {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/de/augusta/dfg/view/AddressView.fxml"));
            AddressController addressController = new AddressController(personListView.getItems());
            fxmlLoader.setController(addressController);
            Parent root1 = fxmlLoader.load();
            Stage stage = new Stage();
            stage.initModality(Modality.WINDOW_MODAL);
            stage.initOwner(((Node) mouseEvent.getSource()).getScene().getWindow());
            stage.setResizable(false);
            stage.setMaxHeight(300);
            stage.setMaxWidth(400);
            stage.setTitle("Adressaufkleber drucken");
            stage.setScene(new Scene(root1));
            stage.getScene().getStylesheets().add(getClass().getResource("/de/augusta/dfg/view/default.css").toExternalForm());
            stage.show();
        } catch (Exception e) {
            String errorText = String.format("Fehler beim öffnen von AdressenView, Fehlermeldung: %s", e.getMessage());
            System.err.println(errorText);
        }
    }

    public void btListsClicked(MouseEvent mouseEvent) {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/de/augusta/dfg/view/ListView.fxml"));
            ListController listController = new ListController(personListView.getItems());
            fxmlLoader.setController(listController);
            Parent root1 = fxmlLoader.load();
            Stage stage = new Stage();
            stage.initModality(Modality.WINDOW_MODAL);
            stage.initOwner(((Node) mouseEvent.getSource()).getScene().getWindow());
            stage.setResizable(false);
            stage.setMaxHeight(500);
            stage.setMaxWidth(900);
            stage.setTitle("Personen Listen");
            stage.setScene(new Scene(root1));
            stage.getScene().getStylesheets().add(getClass().getResource("/de/augusta/dfg/view/default.css").toExternalForm());
            stage.show();
        } catch (Exception e) {
            String errorText = String.format("Fehler beim öffnen von ListenView, Fehlermeldung: %s", e.getMessage());
            e.printStackTrace();
            System.err.println(errorText);
        }
    }

    public void btEmailClicked(MouseEvent mouseEvent) {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/de/augusta/dfg/view/EmailView.fxml"));
            EmailController emailController = new EmailController(personListView.getItems());
            fxmlLoader.setController(emailController);
            Parent root1 = fxmlLoader.load();
            Stage stage = new Stage();
            stage.initModality(Modality.WINDOW_MODAL);
            stage.initOwner(((Node) mouseEvent.getSource()).getScene().getWindow());
            stage.setResizable(false);
            stage.setMaxHeight(500);
            stage.setMaxWidth(700);
            stage.setTitle("e-Mail Verteiler");
            stage.setScene(new Scene(root1));
            stage.getScene().getStylesheets().add(getClass().getResource("/de/augusta/dfg/view/default.css").toExternalForm());
            stage.show();
        } catch (Exception e) {
            String errorText = String.format("Fehler beim öffnen von EmailView, Fehlermeldung: %s", e.getMessage());
            e.printStackTrace();
            System.err.println(errorText);
        }
    }

    public void btMembershipFeeClicked(MouseEvent mouseEvent) {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/de/augusta/dfg/view/MembershipFeeView.fxml"));
            String searchText = textFieldSearchInput.getText();
            btResetSearchClicked();
            List<Person> memberList = personListView.getItems().stream().filter(Person::isMember).collect(Collectors.toList());
            MembershipFeeController membershipFeeControler = new MembershipFeeController(memberList);
            fxmlLoader.setController(membershipFeeControler);
            Parent root1 = fxmlLoader.load();
            Stage stage = new Stage();
            stage.initModality(Modality.WINDOW_MODAL);
            stage.initOwner(((Node) mouseEvent.getSource()).getScene().getWindow());
            stage.setResizable(false);
            stage.setMaxHeight(500);
            stage.setMaxWidth(700);
            stage.setTitle("Beiträge");
            stage.setScene(new Scene(root1));
            stage.getScene().getStylesheets().add(getClass().getResource("/de/augusta/dfg/view/default.css").toExternalForm());
            stage.setOnCloseRequest(event -> {
                PersonManager.getInstance().load();
                restoreSearch(searchText);
            });
            stage.showAndWait();

            if(membershipFeeControler.isEditCanceled()) {
                PersonManager.getInstance().load();
                restoreSearch(searchText);
            }
            else {
                PersonManager.getInstance().save();
            }
        } catch (Exception e) {
            String errorText = String.format("Fehler beim öffnen von MembershipFeeView, Fehlermeldung: %s", e.getMessage());
            e.printStackTrace();
            System.err.println(errorText);
        }
    }

    private void restoreSearch(String suchText) {
        if(suchText.isEmpty()) {
            updatePersonListView();
        }
        else {
            textFieldSearchInput.setText(suchText);
            updatePersonListView(textFieldSearchInput.getText());
        }
    }

    private void updatePersonListView() {
        updatePersonListView(null);
    }

    private void updatePersonListView(String suchText) {
        ObservableList<Person> persons = personListView.getItems();
        persons.clear();
        boolean isMitgliedFilter = toggleButtonMemberFilter.isSelected();

        for (Iterator<Person> personIterator = PersonManager.getInstance().getIterator(); personIterator.hasNext(); ) {
            Person person = personIterator.next();
            if (isMitgliedFilter) {
                if (!person.isMember()) {
                    continue;
                }
            }

            if (suchText != null && !suchText.isEmpty()) {
                if (person.search(suchText)) {
                    persons.add(person);
                }
            } else {
                persons.add(person);
            }
        }

        updateStatusText();
    }

    @SuppressWarnings("SpellCheckingInspection")
    private void updateStatusText() {
        String status = String.format("Anzahl: %d", personListView.getItems().size());

        labelStatus.setText(status);
    }

    private void setEditMode(boolean isEnabled) {
        textFieldTitle.setEditable(isEnabled);
        textFieldFirstName.setEditable(isEnabled);
        textFieldLastName.setEditable(isEnabled);
        datePickerBirthDate.setDisable(!isEnabled);
        textFieldTelephone.setEditable(isEnabled);
        textFieldTelephone2.setEditable(isEnabled);
        textFieldEmail.setEditable(isEnabled);
        textFieldContactPerson.setEditable(isEnabled);
        textFieldAddress.setEditable(isEnabled);
        textFieldZipCode.setEditable(isEnabled);
        textFieldCity.setEditable(isEnabled);
        checkBoxMember.setDisable(!isEnabled);
        if (isEnabled && currentPerson.isMember()) {
            datePickerEntryDate.setDisable(false);
        } else {
            datePickerEntryDate.setDisable(true);
        }
        checkBoxGroupPm.setDisable(!isEnabled);
        checkBoxGroupTeacher.setDisable(!isEnabled);
        checkBoxGroupInfoList.setDisable(!isEnabled);
        checkBoxGroupInterests.setDisable(!isEnabled);
        textAreaNote.setEditable(isEnabled);

        buttonCancel.setDisable(!isEnabled);
        buttonCommit.setDisable(!isEnabled);
        buttonEdit.setDisable(isEnabled);
        buttonNew.setDisable(isEnabled);
        buttonDelete.setDisable(isEnabled);
        buttonAddresses.setDisable(isEnabled);
        buttonLists.setDisable(isEnabled);
        buttonEmail.setDisable(isEnabled);
        buttonMembershipFee.setDisable(isEnabled);

        personListView.setDisable(isEnabled);
    }

    private void updateView() {
        if (currentPerson == null) {
            textFieldTitle.clear();
            textFieldFirstName.clear();
            textFieldLastName.clear();
            datePickerBirthDate.setValue(null);
            textFieldTelephone.clear();
            textFieldTelephone2.clear();
            textFieldEmail.clear();
            textFieldContactPerson.clear();
            textFieldAddress.clear();
            textFieldZipCode.clear();
            textFieldCity.clear();
            checkBoxMember.setSelected(false);
            datePickerEntryDate.setValue(null);
            checkBoxGroupPm.setSelected(false);
            checkBoxGroupTeacher.setSelected(false);
            checkBoxGroupInfoList.setSelected(false);
            checkBoxGroupInterests.setSelected(false);
            textAreaNote.clear();
            return;
        }

        textFieldTitle.setText(currentPerson.getTitle());
        textFieldFirstName.setText(currentPerson.getFirstName());
        textFieldLastName.setText(currentPerson.getLastName());
        datePickerBirthDate.setValue(currentPerson.getBirthDate());
        textFieldTelephone.setText(currentPerson.getTelephone());
        textFieldTelephone2.setText(currentPerson.getTelephone2());
        textFieldEmail.setText(currentPerson.getEmail());
        textFieldContactPerson.setText(currentPerson.getContactPerson());
        textFieldAddress.setText(currentPerson.getAddress());
        textFieldZipCode.setText(currentPerson.getZipCode());
        textFieldCity.setText(currentPerson.getCity());
        if (currentPerson.isMember()) {
            datePickerEntryDate.setValue(currentPerson.getEntryDate());
            checkBoxMember.setSelected(currentPerson.isMember());
        } else {
            datePickerEntryDate.setValue(null);
            datePickerEntryDate.setDisable(true);
            checkBoxMember.setSelected(false);
        }
        checkBoxGroupPm.setSelected(currentPerson.isGruppePm());
        checkBoxGroupTeacher.setSelected(currentPerson.isGroupTeacher());
        checkBoxGroupInfoList.setSelected(currentPerson.isGroupInfoList());
        checkBoxGroupInterests.setSelected(currentPerson.isGroupInterests());
        textAreaNote.setText(currentPerson.getNote());
    }

    private void updateCurrentPerson() {
        currentPerson.setTitle(textFieldTitle.getText());
        currentPerson.setFirstName(textFieldFirstName.getText());
        currentPerson.setLastName(textFieldLastName.getText());
        currentPerson.setBirthDate(datePickerBirthDate.getValue());
        currentPerson.setTelephone(textFieldTelephone.getText());
        currentPerson.setTelephone2(textFieldTelephone2.getText());
        currentPerson.setEmail(textFieldEmail.getText());
        currentPerson.setContactPerson(textFieldContactPerson.getText());
        currentPerson.setAddress(textFieldAddress.getText());
        currentPerson.setZipCode(textFieldZipCode.getText());
        currentPerson.setCity(textFieldCity.getText());
        currentPerson.setMember(checkBoxMember.isSelected());
        if (currentPerson.isMember()) {
            currentPerson.setEntryDate(datePickerEntryDate.getValue());
        } else {
            currentPerson.setEntryDate(null);
        }
        currentPerson.setGruppePm(checkBoxGroupPm.isSelected());
        currentPerson.setGroupTeacher(checkBoxGroupTeacher.isSelected());
        currentPerson.setGroupInfoList(checkBoxGroupInfoList.isSelected());
        currentPerson.setGroupInterests(checkBoxGroupInterests.isSelected());
        currentPerson.setNote(textAreaNote.getText());
    }
}
