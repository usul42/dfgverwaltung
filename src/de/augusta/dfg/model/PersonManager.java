package de.augusta.dfg.model;

import de.augusta.dfg.model.object.MembershipFee;
import de.augusta.dfg.model.object.Person;
import de.augusta.dfg.model.object.PersonList;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.io.File;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

public class PersonManager {
    private static PersonManager personManagerInstance;
    private List<Person> personList;

    private PersonManager() {
        personList = new ArrayList<>();
    }

    public static PersonManager getInstance() {
        if (personManagerInstance == null) {
            personManagerInstance = new PersonManager();
        }
        return personManagerInstance;
    }

    public void addPerson(Person person) {
        int id = 0;

        for (Person p : personList) {
            if (p.getId() > id) {
                id = p.getId();
            }
        }
        person.setId(id + 1);

        personList.add(person);
    }

    public void delPerson(Person person) {
        int index = personList.indexOf(person);

        if (index == -1)
            return;

        personList.remove(index);
    }

    public void editPerson(Person person) {
        int index = personList.indexOf(person);

        if (index == -1)
            return;

        personList.set(index, person);
    }

    public Iterator<Person> getIterator() {
        return personList.iterator();
    }

    public void load() {
        if(ConfigManager.getInstance().isMockUpMode()) {
            personList = MockManager.createPersons(200);

            if(ConfigManager.getInstance().isDebugMode()) {
                System.out.println(String.format("mock mode active, item count = %d", personList.size()));
            }

            return;
        }

        String personFileName = ConfigManager.getInstance().getPersonFile();

        File file = new File(personFileName);
        if(!file.exists()){
            personList = new ArrayList<>();
            return;
        }

        try {
            // create JAXB context and initializing Marshaller
            JAXBContext jaxbContext = JAXBContext.newInstance(PersonList.class);

            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();

            // specify the location and name of xml file to be read
            File xmlFile = new File(personFileName);

            PersonList personList = (PersonList) jaxbUnmarshaller.unmarshal(xmlFile);

            this.personList = personList.getPersons();

            setPersonInfoToMembershipFeeItems();

        } catch (Exception e) {
            System.err.println("Fehler beim laden der Personen-Datei! "+e.toString());
            personList = new ArrayList<>();
        }
    }

    public void save() {
        if(ConfigManager.getInstance().isMockUpMode()){
            if(ConfigManager.getInstance().isDebugMode()) {
                System.out.println("Mock-mode is active: save skipped");
            }
            return;
        }

        String personFileName = ConfigManager.getInstance().getPersonFile();

        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(PersonList.class);
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

            File xmlFile = new File(personFileName);

            PersonList personList = new PersonList();
            personList.setPersons(new ArrayList<>(this.personList));

            jaxbMarshaller.marshal(personList, xmlFile);

        } catch (JAXBException e) {
            e.printStackTrace();
        }
    }

    private void setPersonInfoToMembershipFeeItems() {
        for(Person person : personList) {
            if(person.getMembershipFeeItems() == null)
                continue;

            for(MembershipFee membershipFee : person.getMembershipFeeItems()) {
                membershipFee.setPerson(person);
            }
        }
    }
}