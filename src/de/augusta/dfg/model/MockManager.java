package de.augusta.dfg.model;

import de.augusta.dfg.model.object.MembershipFee;
import de.augusta.dfg.model.object.Person;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

@SuppressWarnings("SpellCheckingInspection")
public class MockManager {
    private static Random random;
    private static List<String> firstNames;
    private static List<String> lastNames;
    private static List<String> eMailDomains;
    private static List<String> areaCode;
    private static List<String> streets;
    private static List<String> cities;
    private static List<String> titles;

    public static List<Person> createPersons(int count) {
        ArrayList<Person> result = new ArrayList<>();

        for (int i = 1; i <= count; i++) {
            Person person = new Person();
            person.setTitle(getRandomTitle());
            person.setFirstName(getRandomFirstName());
            person.setLastName(getRandomLastName());
            if (getRandomBoolean()) {
                person.setMember(true);
                LocalDate today = LocalDate.now();
                person.setEntryDate(today);
                person.setMembershipFeeItems(getRandomMemberFeeItems(person));
            } else {
                person.setMember(false);
            }
            if (i % 2 == 0) {
                person.setEmail(getRandomEmail(person.getFirstName(), person.getLastName()));
            }
            person.setTelephone(getRandomTelephone());
            person.setAddress(getRandomAddress());
            person.setZipCode(getRandomZipCode());
            person.setCity(getRandomOrt());
            person.setBirthDate(getRandomBirthDate());

            result.add(person);
        }

        return result;
    }

    private static ArrayList<MembershipFee> getRandomMemberFeeItems(Person person) {
        ArrayList<MembershipFee> result = new ArrayList<>();

        String year = person.getEntryDate().getYear()+"";
        MembershipFee membershipFee = new MembershipFee();
        membershipFee.setYear(year);
        membershipFee.setPayed(getRandomBoolean());

        result.add(membershipFee);

        return result;
    }

    private static String getRandomZipCode() {
        String zipCode = "";

        for (int i = 0; i < 5; i++) {
            zipCode += getRandomIndex(9);
        }

        return zipCode;
    }

    private static String getRandomAddress() {
        return String.format("%s %d", getRandomStreet(), getRandomIndex(66));
    }

    private static String getRandomTelephone() {
        String telephone = getAreaCode() + " ";

        int telephonLength = 6 + getRandomIndex(4);
        for (int i = 0; i < telephonLength; i++) {
            telephone += getRandomIndex(9);
        }

        return telephone;
    }

    private static String getRandomEmail(String firstName, String lastName) {
        return String.format("%s.%s@%s", firstName, lastName, getRandomEmailDomain());
    }

    private static boolean getRandomBoolean() {
        int seed = getRandomIndex(666);

        return seed % 2 == 0;
    }

    private static int getRandomIndex(int max) {
        if (random == null) {
            random = new Random();
        }

        return random.nextInt(max);
    }

    private static String getRandomFirstName() {
        if (firstNames == null) {
            firstNames = new ArrayList<>(Arrays.asList("Emelina", "Melodi", "Jasmine", "Freeda", "Betsy", "Monica",
                    "Lilly", "Stacee", "Issac", "Vashti", "Pearlie", "Meta", "Sydney", "Kristeen", "Trudie", "Beulah",
                    "Alleen", "Raleigh", "Svetlana", "Chandra", "Steve", "Maria", "Trenton", "Jeanice", "Alina",
                    "Agnes", "Petrina", "Sade", "Fiona", "Alia", "Jayna", "Virgina", "Vernita", "Lannie", "Harriet",
                    "Vonnie", "Felipa", "Renay", "Trudi", "Jeanmarie", "Helen", "Ronny", "Rosetta", "Willie",
                    "Dalila", "Colleen", "Lyda", "Ilda", "Enriqueta", "Thomasina"));
        }
        return firstNames.get(getRandomIndex(firstNames.size()));
    }

    private static String getRandomLastName() {
        if (lastNames == null) {
            lastNames = new ArrayList<>(Arrays.asList("Müller", "Meier", "Schmidt", "Fischer", "Schulz", "Becker",
                    "Hoffmann", "Hansen", "Olsen", "Bauer", "Maier"));
        }
        return lastNames.get(getRandomIndex(lastNames.size()));
    }

    private static String getRandomEmailDomain() {
        if (eMailDomains == null) {
            eMailDomains = new ArrayList<>(Arrays.asList("web.de", "gmx.de", "gmail.com", "yahoo.de", "t-online.de"));
        }
        return eMailDomains.get(getRandomIndex(eMailDomains.size()));
    }

    private static String getAreaCode() {
        if (areaCode == null) {
            areaCode = new ArrayList<>(Arrays.asList("0821", "089", "0177", "0174", "040"));
        }
        return areaCode.get(getRandomIndex(areaCode.size()));
    }

    private static String getRandomStreet() {
        if (streets == null) {
            streets = new ArrayList<>(Arrays.asList("Dorfstr.", "Hauptstraße", "Ottoweg", "Bäckergasse",
                    "Augsburg Straße", "Berliner Platz", "Von Bismark Straße", "Karl Dall All"));
        }
        return streets.get(getRandomIndex(streets.size()));
    }

    private static String getRandomOrt() {
        if (cities == null) {
            cities = new ArrayList<>(Arrays.asList("Aach", "Aachen", "Aalen", "Aarbergen", "Aasbüttel", "Abbenrode",
                    "Abberode", "Abenberg", "Abensberg", "Abentheuer", "Absberg", "Abstatt", "Abtlöbnitz",
                    "Abtsbessingen", "Abtsdorf", "Abtsgmünd", "Abtsteinach", "Abtswind", "Abtweiler", "Achberg",
                    "Achern", "Achim", "Achslach", "Achstetten", "Acht", "Achtelsbach", "Achterwehr", "Achtrup",
                    "Ackendorf", "Adamshoffnung", "Adelberg", "Adelebsen", "Adelheidsdorf", "Adelmannsfelden",
                    "Adelschlag", "Adelsdorf", "Adelsheim", "Adelshofen", "Adelsried", "Adelzhausen", "Adenau",
                    "Adenbach", "Adenbüttel", "Adendorf", "Adenstedt", "Adlkofen", "Admannshagen-Bargeshagen", "Adorf",
                    "Aebtissinwisch", "Aerzen", "Affalterbach", "Affing", "Affinghausen", "Affler", "Agathenburg",
                    "Agethorst", "Aglasterhausen", "Aham", "Ahaus", "Ahausen", "Ahlbeck", "Ahlden ", "Ahlefeld",
                    "Ahlen", "Ahlerstedt", "Ahlsdorf", "Ahlstädt", "Ahlum", "Ahnatal", "Ahneby", "Ahnsbeck", "Ahnsen",
                    "Aholfing", "Aholming", "Ahorn", "Ahorntal", "Ahrbrück", "Ahrensburg", "Ahrensbök", "Ahrensfelde",
                    "Ahrenshagen-Daskow", "Ahrenshoop", "Ahrenshöft", "Ahrenviöl", "Ahrenviölfeld", "Aicha vorm Wald",
                    "Aichach", "Aichelberg", "Aichen", "Aichhalden", "Aichstetten", "Aichtal", "Aichwald", "Aidenbach",
                    "Aidhausen", "Aidlingen", "Aiglsbach", "Ailertchen", "Aindling", "Ainring", "Aislingen",
                    "Aiterhofen", "Aitern", "Aitrach", "Aitrang", "Aken ", "Albaching", "Albbruck", "Albersdorf",
                    "Albershausen", "Albersroda", "Alberstedt", "Albersweiler", "Albertshofen", "Albessen", "Albig",
                    "Albisheim ", "Albsfelde", "Albstadt", "Aldenhoven", "Aldersbach", "Aldingen", "Alerheim",
                    "Alesheim", "Aletshausen", "Alf", "Alfdorf", "Alfeld", "Alfeld ", "Alfhausen", "Alflen",
                    "Alfstedt", "Alfter", "Algenstedt", "Algermissen", "Alheim", "Alken", "Alkersleben", "Alkersum",
                    "Allenbach", "Allendorf", "Allendorf ", "Allenfeld", "Allensbach", "Alleringersleben",
                    "Allersberg", "Allershausen", "Alleshausen", "Alling", "Allmannshofen", "Allmannsweiler",
                    "Allmendingen", "Allmersbach im Tal", "Allrode", "Allstedt", "Almdorf", "Almersbach", "Almstedt",
                    "Alpen", "Alpenrod", "Alperstedt", "Alpirsbach", "Alsbach", "Alsbach-Hähnlein", "Alsdorf", "Alsenz",
                    "Alsfeld", "Alsheim", "Alsleben ", "Alt Bennebek", "Alt Bukow", "Alt Duvenstedt", "Alt Krenzlin",
                    "Alt Meteln", "Alt Mölln", "Alt Rehse", "Alt Schwerin", "Alt Schönau", "Alt Sührkow", "Alt Tellin",
                    "Alt Tucheband", "Alt Zachun", "Alt Zauche-Wußwerk", "Altbach", "Altbrandsleben", "Altdorf",
                    "Altdorf b. Nürnberg", "Altdöbern", "Altefähr", "Alteglofsheim", "Altena", "Altenahr", "Altenau",
                    "Altenbamberg", "Altenbeken", "Altenberg", "Altenberga", "Altenberge", "Altenbeuthen", "Altenbrak",
                    "Altenbuch", "Altenburg", "Altendiez", "Altendorf", "Altenfeld", "Altenglan", "Altengottern",
                    "Altenhagen", "Altenhausen", "Altenhof", "Altenholz", "Altenkirchen", "Altenkirchen ",
                    "Altenkrempe", "Altenkunstadt", "Altenmarkt a. d. Alz", "Altenmedingen", "Altenmoor",
                    "Altenmünster", "Altenpleen", "Altenriet", "Altenroda", "Altensalzwedel", "Altenstadt",
                    "Altenstadt a. d. Waldnaab", "Altensteig", "Altenthann", "Altentreptow", "Altenzaun", "Alterkülz",
                    "Alterode", "Altersbach", "Altertheim", "Altfraunhofen", "Althegnenberg", "Altheim", "Altheim ",
                    "Althengstett", "Althornbach", "Althütte", "Althüttendorf", "Altjeßnitz", "Altkalen", "Altkirchen",
                    "Altlandsberg", "Altlay", "Altleiningen", "Altlußheim", "Altmannstein", "Altmersleben",
                    "Altmittweida", "Altomünster", "Altrich", "Altrip", "Altscheid", "Altshausen", "Altstrimmig",
                    "Altusried", "Altwarp", "Altweidelbach", "Altwigshagen", "Altötting", "Alveslohe",
                    "Alzenau i. UFr.", "Alzey", "Am Großen Bruch", "Am Großen Rhode", "Am Mellensee", "Am Salzhaff"));
        }
        return cities.get(getRandomIndex(cities.size()));
    }

    private static LocalDate getRandomBirthDate() {
        int day = 0;
        while (day == 0) {
            day = getRandomIndex(29);
        }
        int month = 0;
        while (month == 0) {
            month = getRandomIndex(12);
        }
        int year = 0;
        while (year < 1955) {
            year = getRandomIndex(2000);
        }

        return LocalDate.of(year, month, day);
    }

    private static String getRandomTitle() {
        if (titles == null) {
            titles = new ArrayList<>(Arrays.asList("Dr.", "Prof.", null, null, null, null, null, null, null, null));
        }
        return titles.get(getRandomIndex(titles.size()));
    }
}
