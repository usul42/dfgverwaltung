package de.augusta.dfg.model.enums;

public enum OsType {
    Unknown,
    Windows,
    Linux,
    MacOS
}
