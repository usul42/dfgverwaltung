package de.augusta.dfg.model.enums;

public enum EmailFilter {
    All,
    Member,
    Pm,
    Teacher,
    InfoList,
    Interested
}
