package de.augusta.dfg.model;

import de.augusta.dfg.model.enums.EmailFilter;
import de.augusta.dfg.model.enums.OsType;
import de.augusta.dfg.model.object.DfgConfig;

import javax.swing.filechooser.FileSystemView;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.nio.file.Paths;


public class ConfigManager {
    private static ConfigManager configManager;
    private final String dataFile;
    private final String configFile;
    private DfgConfig dfgConfig;
    private ArgumentUtils argumentUtils;

    @SuppressWarnings("SpellCheckingInspection")
    private ConfigManager() {
        dataFile = "personen.xml";
        configFile = "dfgconfig.xml";
        load();
    }

    public static ConfigManager getInstance() {
        if (configManager == null) {
            configManager = new ConfigManager();
        }
        return configManager;
    }

    public String getEmailSubject(EmailFilter emailFilter) {
        if (ConfigManager.getInstance().isMockUpMode()) {
            return "Mock dummy subject for " + emailFilter.toString();
        }

        if (dfgConfig == null) {
            return null;
        }

        switch (emailFilter) {
            case All:
                return dfgConfig.getEmailSubjectAll();
            case InfoList:
                return dfgConfig.getEmailSubjectInfo();
            case Interested:
                return dfgConfig.getEmailSubjectInterested();
            case Teacher:
                return dfgConfig.getEmailSubjectTeacher();
            case Pm:
                return dfgConfig.getEmailSubjectPm();
            case Member:
                return dfgConfig.getEmailSubjectMember();
        }

        return null;
    }

    public void setEmailSubject(EmailFilter emailFilter, String subject) {
        if (dfgConfig == null) {
            return;
        }

        switch (emailFilter) {
            case All:
                dfgConfig.setEmailSubjectAll(subject);
                break;
            case InfoList:
                dfgConfig.setEmailSubjectInfo(subject);
                break;
            case Interested:
                dfgConfig.setEmailSubjectInterested(subject);
                break;
            case Teacher:
                dfgConfig.setEmailSubjectTeacher(subject);
                break;
            case Pm:
                dfgConfig.setEmailSubjectPm(subject);
                break;
            case Member:
                dfgConfig.setEmailSubjectMember(subject);
                break;
        }

        save();
    }

    public void setArguments(ArgumentUtils argumentUtils) {
        this.argumentUtils = argumentUtils;
    }

    public boolean isMockUpMode() {
        return this.argumentUtils.isMockMode();
    }

    public boolean isDebugMode() {
        return this.argumentUtils.isDebugMode();
    }

    OsType getOsType() {
        String os = System.getProperty("os.name").toLowerCase();

        if (os.contains("win")) {
            return OsType.Windows;
        }
        if (os.contains("mac")) {
            return OsType.MacOS;
        }
        if (os.contains("nix") || os.contains("nux")) {
            return OsType.Linux;
        }

        return OsType.Unknown;
    }

    @SuppressWarnings("SpellCheckingInspection")
    private String getDataPath() {
        String dataDir;
        String homePath;

        switch (getOsType()) {
            case Windows:
                homePath = FileSystemView.getFileSystemView().getDefaultDirectory().getPath();
                dataDir = "DFG_Daten";
                break;
            case Linux:
            case MacOS:
                homePath = System.getProperty("user.home");
                dataDir = ".dfg_daten";
                break;
            default:
                throw new IllegalArgumentException("Invalid os type: "+getOsType());
        }

        String dataPath = Paths.get(homePath, dataDir).toString();

        //noinspection ResultOfMethodCallIgnored
        new File(dataPath).mkdir();

        return dataPath;
    }

    String getPersonFile() {
        if(isDebugMode()) {
            System.out.println("getPersonFile(): "+getPersonFile(dataFile));
        }
        return getPersonFile(dataFile);
    }

    private String getPersonFile(String filename) {
        return Paths.get(getDataPath(), filename).toString();
    }

    private String getConfigFile() {
        return Paths.get(getDataPath(), configFile).toString();
    }

    private void load() {
        String configFilename = getConfigFile();

        File file = new File(configFilename);
        if (!file.exists()) {
            dfgConfig = new DfgConfig();
            return;
        }

        try {
            // create JAXB context and initializing Marshaller
            JAXBContext jaxbContext = JAXBContext.newInstance(DfgConfig.class);

            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();

            // specify the location and name of xml file to be read
            File xmlFile = new File(configFilename);

            dfgConfig = (DfgConfig) jaxbUnmarshaller.unmarshal(xmlFile);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void save() {
        String configFilename = getConfigFile();

        if (isMockUpMode()) {
            if(isDebugMode()) {
                System.out.println("Mock up mode is active: save skipped");
            }
            return;
        }

        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(DfgConfig.class);
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

            File xmlFile = new File(configFilename);

            jaxbMarshaller.marshal(dfgConfig, xmlFile);

        } catch (JAXBException e) {
            e.printStackTrace();
        }
    }
}
