package de.augusta.dfg.model;

import de.augusta.dfg.model.object.Person;

import java.util.List;

public class PrintManager {
    private static PrintManager printManager;

    private PrintManager() {

    }

    public static PrintManager getInstance() {
        if (printManager == null) {
            printManager = new PrintManager();
        }
        return printManager;
    }

    public void printAddresses(List<Person> data) {
        System.out.println("do print stuff for " + data.size() + " items ...");
    }

    public void printPersonDetails(List<Person> data) {
        System.out.println("do print stuff for " + data.size() + " items ...");
    }

    public void printMembershipFeeList(List<Person> data) {
        System.out.println("do print stuff for " + data.size() + " items ...");
    }

    public void printList(List<Person> data) {
        System.out.println("do print stuff for " + data.size() + " items ...");
    }

    public void printPersonList(List<Person> data) {
        System.out.println("do print stuff for " + data.size() + " items ...");
    }
}
