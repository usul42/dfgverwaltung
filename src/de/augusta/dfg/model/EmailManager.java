package de.augusta.dfg.model;

import de.augusta.dfg.model.enums.OsType;
import de.augusta.dfg.model.object.Person;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class EmailManager {
    private static EmailManager emailManager;

    private EmailManager() {

    }

    public static EmailManager getInstance() {
        if (emailManager == null) {
            emailManager = new EmailManager();
        }
        return emailManager;
    }

    public List<String> emailList(List<Person> persons, String subject) {
        List<String> result = new ArrayList<>();

        int step = 50;
        int count = persons.size();

        for (int index = 0; index <= count; index += step) {
            List<Person> sublist = new ArrayList<>();

            int rest = count - index;

            int subMax;
            if (rest < step) {
                subMax = index + rest;
            } else {
                subMax = index + step;
            }
            for (int subIndex = index; subIndex < subMax; subIndex++) {
                sublist.add(persons.get(subIndex));
            }

            String mailToLink = createEmailLink(sublist, subject);

            result.add(mailToLink);
        }

        return result;
    }

    public void sendMails(List<String> mailToLinks) {
        try {
            Runtime runtime = Runtime.getRuntime();
            OsType osType = ConfigManager.getInstance().getOsType();
            for (String mailTo : mailToLinks) {
                switch (osType) {
                    case Windows:
                        runtime.exec("rundll32 url.dll,FileProtocolHandler " + mailTo);
                        break;
                    case MacOS:
                        runtime.exec("open " + mailTo);
                        break;
                    // TODO: rewrite
//                    case Linux:
//                        String[] browsers = {"epiphany", "firefox", "mozilla", "konqueror",
//                                "netscape", "opera", "links", "lynx"};
//
//                        StringBuilder cmd = new StringBuilder();
//                        for (int i = 0; i < browsers.length; i++) {
//                            cmd.append((i == 0 ? "" : " || ") + browsers[i] + " \"" + mailTo + "\" ");
//                        }
//
//                        runtime.exec(new String[]{"sh", "-c", cmd.toString()});
//                        break;
                    default:
                        throw new IllegalArgumentException("Invalid OS-Type: '"+osType+"+");
                }
            }
        } catch (IOException ex) {
            System.err.println("Fehler beim verschicken der e-Mails! Fehler: " + ex);
        }
    }

    private String createEmailLink(List<Person> persons, String subject) {
        if (subject == null) {
            subject = "Info Mail";
        }

        String mailTo = "mailto:?bcc=";

        for (Person person : persons) {
            mailTo += person.getEmail() + ",";
        }

        // Remove last semikolon
        mailTo = mailTo.substring(0, mailTo.length() - 1);

        mailTo += "&subject=" + subject.replace(" ", "%20");

        return mailTo;
    }
}
