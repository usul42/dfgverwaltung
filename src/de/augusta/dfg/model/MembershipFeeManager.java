package de.augusta.dfg.model;

import de.augusta.dfg.model.object.MembershipFee;
import de.augusta.dfg.model.object.Person;

import java.util.HashMap;

public class MembershipFeeManager {
    private static MembershipFeeManager instance;
    private HashMap<Integer, MembershipFee> hashCodeMembershipFeeMap;

    private MembershipFeeManager() {
        resetCache();
    }

    public static MembershipFeeManager getInstance() {
        if (instance == null) {
            instance = new MembershipFeeManager();
        }
        return instance;
    }

    public void resetCache() {
        hashCodeMembershipFeeMap = new HashMap<>();
    }

    public MembershipFee getOrCreateMembershipFee(Person person, String year) {
        int hashCode = person.hashCode() + year.hashCode();

        if(hashCodeMembershipFeeMap.containsKey(hashCode)){
           return hashCodeMembershipFeeMap.get(hashCode);
        }
        else {
            MembershipFee membershipFee = new MembershipFee(person, year);
            hashCodeMembershipFeeMap.put(hashCode, membershipFee);
            return membershipFee;
        }
    }
}
