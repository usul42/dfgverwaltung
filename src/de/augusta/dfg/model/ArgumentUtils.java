package de.augusta.dfg.model;

public class ArgumentUtils {
    private boolean debugMode;
    private boolean mockMode;

    @SuppressWarnings("SpellCheckingInspection")
    public ArgumentUtils(String[] args) {
        for (String arg : args) {
            if (arg.contains("-mockup")) {
                mockMode = true;
            }
            if(arg.contains("-debug")) {
                debugMode = true;
            }
        }
    }

    boolean isMockMode() {
        return mockMode;
    }

    boolean isDebugMode() { return debugMode; }
}
