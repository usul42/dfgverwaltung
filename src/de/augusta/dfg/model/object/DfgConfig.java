package de.augusta.dfg.model.object;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "DfgConfig")
public class DfgConfig {
    private String emailSubjectAll;
    private String emailSubjectMember;
    private String emailSubjectInterested;
    private String emailSubjectInfo;
    private String emailSubjectTeacher;
    private String emailSubjectPm;


    public String getEmailSubjectAll() {
        return emailSubjectAll;
    }

    @XmlElement(name = "EmailSubjectAlle")
    public void setEmailSubjectAll(String emailSubjectAll) {
        this.emailSubjectAll = emailSubjectAll;
    }

    public String getEmailSubjectMember() {
        return emailSubjectMember;
    }

    @XmlElement(name = "EmailSubjectMitglieder")
    public void setEmailSubjectMember(String emailSubjectMember) {
        this.emailSubjectMember = emailSubjectMember;
    }

    public String getEmailSubjectInterested() {
        return emailSubjectInterested;
    }

    @XmlElement(name = "EmailSubjectInteresierte")
    public void setEmailSubjectInterested(String emailSubjectInterested) {
        this.emailSubjectInterested = emailSubjectInterested;
    }

    public String getEmailSubjectInfo() {
        return emailSubjectInfo;
    }

    @XmlElement(name = "EmailSubjectInfo")
    public void setEmailSubjectInfo(String emailSubjectInfo) {
        this.emailSubjectInfo = emailSubjectInfo;
    }

    public String getEmailSubjectTeacher() {
        return emailSubjectTeacher;
    }

    @XmlElement(name = "EmailSubjectLehrer")
    public void setEmailSubjectTeacher(String emailSubjectTeacher) {
        this.emailSubjectTeacher = emailSubjectTeacher;
    }

    public String getEmailSubjectPm() {
        return emailSubjectPm;
    }

    @XmlElement(name = "EmailSubjectPm")
    public void setEmailSubjectPm(String emailSubjectPm) {
        this.emailSubjectPm = emailSubjectPm;
    }
}
