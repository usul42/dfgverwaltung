package de.augusta.dfg.model.object;

import javax.xml.bind.annotation.XmlAttribute;

public class MembershipFee {
    private Person person;
    private boolean isPayed;
    private String year;

    // constructor for xml parsing
    @SuppressWarnings("unused")
    public MembershipFee() {
    }

    public MembershipFee(Person person, String year) {
        this.person = person;
        this.year = year;

        for(MembershipFee membershipFee : person.getMembershipFeeItems()){
            if(membershipFee.getYear().equals(year)) {
                setPayed(membershipFee.isPayed());
                break;
            }
        }
    }

    public String getDisplayName() {
        return this.person.getFullName();
    }

    public boolean isPayed() {
        return this.isPayed;
    }

    public String getPayedText() {
        if(isPayed) {
            return "bezahlt";
        }
        return "nicht bezahlt";
    }

    @XmlAttribute(name = "Bezahlt")
    public void setPayed(boolean isPayed) {
        this.isPayed = isPayed;
    }

    public String getYear() {
        return this.year;
    }

    @XmlAttribute(name = "Jahr")
    public void setYear(String year) {
        this.year = year;
    }

    public int getPersonId() {
        return this.person.getId();
    }

    @Override
    public int hashCode() {
        return this.person.hashCode() + this.year.hashCode();
    }

    public void setPerson(Person person) {
        this.person = person;
    }
}
