package de.augusta.dfg.model.object;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;


@SuppressWarnings("SpellCheckingInspection")
@XmlRootElement(name = "PersonenListe")
public class PersonList {
    private ArrayList<Person> persons;

    public PersonList() {

    }

    public ArrayList<Person> getPersons() {
        return persons;
    }

    @XmlElementWrapper(name = "Personen")
    @XmlElement(name = "Person")
    public void setPersons(ArrayList<Person> persons) {
        this.persons = persons;
    }
}
