package de.augusta.dfg.model.object;

import javax.xml.bind.annotation.adapters.XmlAdapter;
import java.time.LocalDate;

class LocalDateAdapter extends XmlAdapter<String, LocalDate> {
    public LocalDate unmarshal(String value) throws Exception {
        // Das XML von der .NET Anwendung speichert zum Datum noch zusätzlich die Uhrzeit in Form T00:00:00
        // LocalDate.parse will nur das Datum ohne Uhrzeit
        String temp[] = value.split("T");

        return LocalDate.parse(temp[0]);
    }

    public String marshal(LocalDate value) throws Exception {
        return value.toString();
    }
}
