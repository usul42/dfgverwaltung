package de.augusta.dfg.model.object;

import java.time.LocalDate;
import java.util.ArrayList;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

@XmlRootElement
public class Person {
    private int id;
    private String note;
    private String title;
    private String firstName;
    private String lastName;
    private LocalDate birthDate;
    private LocalDate entryDate;
    private String telephone;
    private String telephone2;
    private String email;
    private String address;
    private String zipCode;
    private String city;
    private Boolean member;
    private String contactPerson;

    private Boolean groupPm;
    private Boolean groupTeacher;
    private Boolean groupInfoList;
    private Boolean groupInterests;
    private ArrayList<MembershipFee> membershipFeeItems;

    public Person() {
        member = false;
        groupPm = false;
        groupTeacher = false;
        groupInfoList = false;
        groupInterests = false;
    }

    public int getId() {
        return id;
    }

    @XmlElement(name = "ID")
    public void setId(int id) {
        this.id = id;
    }

    public String getNote() {
        return note;
    }

    @XmlElement(name = "Notiz")
    public void setNote(String note) {
        this.note = note;
    }

    public String getTitle() {
        return title;
    }

    @XmlElement(name = "Titel")
    public void setTitle(String title) {
        this.title = title;
    }

    public String getFirstName() {
        return firstName;
    }

    @XmlElement(name = "Vorname")
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    @XmlElement(name = "Nachname")
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getTelephone() {
        return telephone;
    }

    @XmlElement(name = "Telefon")
    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getTelephone2() {
        return telephone2;
    }

    @XmlElement(name = "Telefon2")
    public void setTelephone2(String telephone2) {
        this.telephone2 = telephone2;
    }

    public String getEmail() {
        return email;
    }

    @XmlElement(name = "Email")
    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    @XmlElement(name = "Adresse")
    public void setAddress(String address) {
        this.address = address;
    }

    public String getZipCode() {
        return zipCode;
    }

    @XmlElement(name = "PLZ")
    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getCity() {
        return city;
    }

    @XmlElement(name = "Ort")
    public void setCity(String city) {
        this.city = city;
    }

    public Boolean isMember() {
        return member;
    }

    @XmlElement(name = "Mitglied")
    public void setMember(Boolean member) {
        this.member = member;
    }

    public String getContactPerson() {
        return contactPerson;
    }

    @XmlElement(name = "Ansprechpartner")
    public void setContactPerson(String contactPerson) {
        this.contactPerson = contactPerson;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    @XmlJavaTypeAdapter(value = LocalDateAdapter.class)
    @XmlElement(name = "GeburtsDatumDT")
    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }

    public LocalDate getEntryDate() {
        return entryDate;
    }

    @XmlJavaTypeAdapter(value = LocalDateAdapter.class)
    @XmlElement(name = "EintrittsDatumDT")
    public void setEntryDate(LocalDate entryDate) {
        this.entryDate = entryDate;
    }

    public Boolean isGruppePm() {
        return groupPm;
    }

    @XmlElement(name = "IsGruppePm")
    public void setGruppePm(Boolean isGroupPm) {
        this.groupPm = isGroupPm;
    }

    public Boolean isGroupTeacher() {
        return groupTeacher;
    }

    @XmlElement(name = "IsGruppeInfoVerteiler")
    public void setGroupTeacher(Boolean isGroupTeacher) {
        this.groupTeacher = isGroupTeacher;
    }

    public Boolean isGroupInfoList() {
        return groupInfoList;
    }

    @XmlElement(name = "IsGruppeInteresierte")
    public void setGroupInfoList(Boolean isGroupInfoList) {
        this.groupInfoList = isGroupInfoList;
    }

    public Boolean isGroupInterests() {
        return groupInterests;
    }

    @XmlElement(name = "IsGruppeInteresierte")
    public void setGroupInterests(Boolean isGroupInterests) {
        this.groupInterests = isGroupInterests;
    }

    public ArrayList<MembershipFee> getMembershipFeeItems() {
        return membershipFeeItems;
    }

    @XmlElementWrapper(name = "Beitraege")
    @XmlElement(name = "Beitrag")
    public void setMembershipFeeItems(ArrayList<MembershipFee> membershipFeeItems) {
        this.membershipFeeItems = membershipFeeItems;
    }

    @Override
    public String toString() {
        String tempMember = "";
        if (member) {
            tempMember = "M";
        }

        return String.format("%-3s%s", tempMember, getFullName());
    }

    public String getFullName() {
        String tempTitle = "";
        if (title != null && !title.trim().isEmpty()) {
            tempTitle = title.trim() + " ";
        }

        String tempFirstName = "";
        if (firstName != null && !firstName.trim().isEmpty()) {
            tempFirstName = firstName.trim() + " ";
        }

        String tempLastName = "";
        if (lastName != null && !lastName.trim().isEmpty()) {
            tempLastName = lastName.trim();
        }

        return String.format("%s%s%s", tempTitle, tempFirstName, tempLastName);
    }

    public boolean search(String searchText) {
        searchText = searchText.toLowerCase();
        boolean found = false;

        if (firstName != null && firstName.toLowerCase().contains(searchText)) {
            found = true;
        }

        if (lastName != null && lastName.toLowerCase().contains(searchText)) {
            found = true;
        }

        if (telephone != null) {
            String telephonTemp = telephone.replace(" ", "").replace("/", "");
            String searchTextTemp = searchText.replace(" ", "").replace("/", "");
            found = telephonTemp.contains(searchTextTemp);
        }

        if (telephone2 != null) {
            String telephone2Temp = telephone2.replace(" ", "").replace("/", "");
            String searchTextTemp = searchText.replace(" ", "").replace("/", "");
            found = telephone2Temp.contains(searchTextTemp);
        }

        return found;
    }

    public boolean hasEmail() {
        return email != null && !email.trim().isEmpty();
    }

    @SuppressWarnings("unused")
    public String getMemberText() {
        if (member) {
            return "Ja";
        }
        return "Nein";
    }

    public void updateMembershipFee(MembershipFee membershipFee) {
        int index = getMembershipFeeIndex(membershipFee);
        if(index == -1) {
            return;
        }

        getMembershipFeeItems().set(index, membershipFee);
    }

    private int getMembershipFeeIndex(MembershipFee membershipFee) {
        int hashCode = membershipFee.hashCode();

        if (getMembershipFeeItems() == null) {
            return -1;
        }

        for(MembershipFee item : getMembershipFeeItems()) {
            if(item.hashCode() == hashCode) {
                return getMembershipFeeItems().indexOf(item);
            }
        }

        return -1;
    }
}
