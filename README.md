# README

Addressen- und Mitgliederverwaltung für die Deutsch-Französische Gesellschaft (DFG)

## Parameter

### -mockup 

Erzeugt 200 Demo Datensätze für Personen welche zufällig zusätzlich als Mitglieder ergänzt sind

### -debug

Gibt zur Laufzeit zusätzliche Debug-Meldungen aus